export const setTimeoutPromise = (milliseconds: number) => {
  return new Promise((resolve, reject) => setTimeout(() => resolve(), milliseconds));
}