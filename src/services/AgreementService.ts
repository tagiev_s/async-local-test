import { setTimeoutPromise } from "../helper/Helper"
import { IAgreement } from "../model/IAgreement";

export class AgreementService {
  public async getAgreementsByUser(userId: number): Promise<IAgreement[]> {
    await setTimeoutPromise(300);

    if(userId === 1) {
      return [{
        id: 1,
        productId: 29,
        userId: 1,
        count: 3,
        price: 5,
        totalPrice: 150
      }]
    }
    if(userId === 2) {
      return [{
        id: 2,
        productId: 14,
        userId: 2,
        count: 5,
        price: 100,
        totalPrice: 500
      }]
    }
    return [];
  }
}