import { setTimeoutPromise } from "../helper/Helper"
import { IUser } from "../model/IUser";

export class UserService {
  public async getUser(): Promise<IUser[]> {
    await setTimeoutPromise(500);
    return [
      {
        id: 1,
        email: "test@example.com",
        firstName: "Иван",
        lastName: "Сидоров"
      },
      {
        id: 2,
        email: "test2@example.com",
        firstName: "Василий",
        lastName: "Петров"
      }
    ]
  }
}