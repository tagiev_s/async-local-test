const globalAsyncContextName = "globalAsyncContext";
const OldPromise = Promise;

declare var global: any;

class AsyncLocalPromise<T> extends OldPromise<T> {
  private globalAsyncContext: any | undefined;
  constructor(executor: (resolve: (value?: T | PromiseLike<T>) => void, reject: (reason?: any) => void) => void) {
    super(executor);
    this.globalAsyncContext = { ... global[globalAsyncContextName] };
  }

  public then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | PromiseLike<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | PromiseLike<TResult2>) | undefined | null) {
    return super.then(this.wrappedContext(onfulfilled), this.wrappedContext(onrejected));
  }

  public catch<TResult = never>(onrejected?: ((reason: any) => TResult | PromiseLike<TResult>) | undefined | null) {
    return super.catch(this.wrappedContext(onrejected));
  }

  private wrappedContext<TResult>(callback?: ((value: T) => TResult | PromiseLike<TResult>) | undefined | null) {
    if (callback) {
      return (value: T) => {
        const oldContext = global[globalAsyncContextName];
        try {
          global[globalAsyncContextName] = this.globalAsyncContext;
          const res = callback(value);
          return res;
        } finally {
          global[globalAsyncContextName] = oldContext;
        }
      }
    }
  }
}

Promise = AsyncLocalPromise;

export const setAsyncLocal = (key: string, value: any) => {
  if (!global[globalAsyncContextName]) {
    global[globalAsyncContextName] = {};
  }
  global[globalAsyncContextName][key] = value;
}

export const getAsyncLocal = (key: string) => {
  return global[globalAsyncContextName] ? global[globalAsyncContextName][key] : undefined;
}
