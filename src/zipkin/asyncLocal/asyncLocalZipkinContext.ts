import {getAsyncLocal, setAsyncLocal} from './index';

export class AsyncLocalZipkinContext {
  constructor() {
    setAsyncLocal('zipkin-context', null);
  }

  public setContext(ctx: any) {
    setAsyncLocal('zipkin-context', ctx);
  }

  public getContext() {
    return getAsyncLocal('zipkin-context');
  }

  public scoped(callable: () => any) {
    const prevCtx = this.getContext();
    try {
      return callable();
    } finally {
      this.setContext(prevCtx);
    }
  }

  public letContext(ctx: any, callable: () => any) {
    return this.scoped(() => {
      this.setContext(ctx);
      return callable();
    });
  }
};