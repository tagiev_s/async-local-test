import * as zipkin from 'zipkin';
import { HttpLogger } from 'zipkin-transport-http';
import { AsyncLocalZipkinContext } from './asyncLocal/asyncLocalZipkinContext';

export const tracer = new zipkin.Tracer({
  ctxImpl: new AsyncLocalZipkinContext(),
  recorder: new zipkin.BatchRecorder({
    logger: new HttpLogger({
      endpoint: 'http://localhost:9411/api/v2/spans',
      jsonEncoder: zipkin.jsonEncoder.JSON_V2,
    })
  }),
  localServiceName: 'partner',
});

export function traceable(name?: string) {
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    if (descriptor.value) {
      const method = descriptor.value;
      descriptor.value = function (...args: any[]) {
        const _this = this;
        const methodName = name || method.name || 'no-name-function';
        console.log(methodName + ' execute');
        const result = tracer.local(methodName, () => method.apply(_this, args));
        return result;
      }
    }
    return descriptor;
  };
}