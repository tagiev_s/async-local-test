import { UserService } from "../services/UserService";
import { AgreementService } from "../services/AgreementService";
import { IAgreement } from "../model/IAgreement";

export class ReportTest {
  constructor(
    private userService: UserService,
    private agreementService: AgreementService
  ) {}

  public async getTotalReport() {
    const users = await this.userService.getUser();
    const reports: any[] = [];
    users.forEach(async user => {
      const agreements = await this.agreementService.getAgreementsByUser(user.id);
      reports.push(... agreements.map(x => this.getReport(x)));
    });
    return reports;
  }

  private getReport(agreement: IAgreement) {
    console.log(agreement.totalPrice);
    return {};
  }
}
