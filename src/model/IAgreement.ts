export interface IAgreement {
  id: number;
  productId: number;
  userId: number;
  count: number;
  price: number;
  totalPrice: number;
}